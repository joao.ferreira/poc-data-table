import { PocDataTablePage } from './app.po';

describe('poc-data-table App', function() {
  let page: PocDataTablePage;

  beforeEach(() => {
    page = new PocDataTablePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
