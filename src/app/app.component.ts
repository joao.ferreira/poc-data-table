import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Data Table POC';

  rows = [
    { name: 'João', genero: 'Masculino', empresa: 'Mc Donalds'},
    { name: 'Caio', genero: 'Masculino', empresa: 'KFC' },
    { name: 'Juliana', genero: 'Feminino', empresa: 'Burger King' },
    { name: 'Caroline', genero: 'Feminino', empresa: 'Outback' },
    { name: 'Maria', genero: 'Feminino', empresa: 'Apple' },
    { name: 'Beatriz', genero: 'Feminino', empresa: 'Microsoft' },
    { name: 'Ana Luiza', genero: 'Feminino', empresa: 'Dell' },
    { name: 'Lucas', genero: 'Masculino', empresa: 'HP' },
    { name: 'Fernanda', genero: 'Feminino', empresa: 'Lenovo' },
    { name: 'Janaina', genero: 'Feminino', empresa: 'Adidas' },
    { name: 'Giovana', genero: 'Feminino', empresa: 'Nike' },
    { name: 'Gabriele', genero: 'Feminino', empresa: 'Vans' },
    { name: 'Rafaele', genero: 'Feminino', empresa: 'ADTsys' },
    { name: 'Khaleesi', genero: 'Feminino', empresa: 'Dragon' },
    { name: 'Jhon Snow', genero: 'Masculino', empresa: 'Westeros' },
  ];
  columns = [
    { prop: 'name' },
    { prop: 'genero' },
    { prop: 'empresa' }
  ];
}
